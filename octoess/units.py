#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2020 Philip Withnall
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA


from decimal import *


class Duration:
    def __init__(self, seconds=None, minutes=None):
        # Internally store in seconds
        if seconds and minutes:
            raise ValueError('Only one unit can be specified')

        if seconds is not None:
            self.__seconds = int(seconds)
        elif minutes is not None:
            self.__seconds = int(minutes * 60)
        else:
            raise ValueError('Duration units have to be specified')


    def minutes(self):
        return self.__seconds * 60


    def __eq__(self, other):
        if isinstance(other, Duration):
            return self.__seconds == other.__seconds
        else:
            raise ValueError('Unsupported operand types')


class Energy:
    def __init__(self, wm=None, kwh=None):
        # Internally store in watt-minutes
        if wm and kwh:
            raise ValueError('Only one unit can be specified')

        if wm is not None:
            self.__wm = int(wm)
        elif kwh is not None:
            self.__wm = int(kwh * 60.0 * 1000.0)
        else:
            raise ValueError('Energy units have to be specified')


    def __str__(self):
        return str(self.__wm) + 'Wm'


    def __gt__(self, other):
        if isinstance(other, Energy):
            return self.__wm > other.__wm
        else:
            raise ValueError('Unsupported operand types')


    def __le__(self, other):
        if isinstance(other, Energy):
            return self.__wm <= other.__wm
        else:
            raise ValueError('Unsupported operand types')


    def __mul__(self, other):
        if isinstance(other, float):
            return Energy(wm=self.__wm * other)
        else:
            raise ValueError('Unsupported operand types')


    __rmul__ = __mul__


    def __sub__(self, other):
        if isinstance(other, Energy):
            return Energy(wm=self.__wm - other.__wm)
        else:
            raise ValueError('Unsupported operand types')


class EnergyPrice:
    def __init__(self, p_kwh=None):
        # Internally store in p/kWh to 2dp
        self.__p_kwh = Decimal(p_kwh).quantize(Decimal('.01'), rounding=ROUND_UP)


class Power:
    def __init__(self, watts=None, kilowatts=None):
        # Internally store in watts
        if watts and kilowatts:
            raise ValueError('Only one unit can be specified')

        if watts is not None:
            self.__watts = int(watts)
        elif kilowatts is not None:
            self.__watts = int(1000.0 * kilowatts)
        else:
            raise ValueError('Power units have to be specified')


    def __abs__(self):
        return Power(watts=abs(self.__watts))


    def __ge__(self, other):
        if isinstance(other, Power):
            return self.__watts >= other.__watts
        else:
            raise ValueError('Unsupported operand types')


    def __lt__(self, other):
        if isinstance(other, Power):
            return self.__watts < other.__watts
        else:
            raise ValueError('Unsupported operand types')


    def __mul__(self, other):
        if isinstance(other, Duration):
            return Energy(wm=self.__watts * other.minutes())
        else:
            raise ValueError('Unsupported operand types')


    __rmul__ = __mul__


    def __sub__(self, other):
        if isinstance(other, Power):
            return Power(watts=self.__watts - other.__watts)
        else:
            raise ValueError('Unsupported operand types')
