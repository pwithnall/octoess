#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2020 Philip Withnall
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

import datetime
from itertools import product
from operator import itemgetter

from .modelstate import ModelState
from .units import *


class Model:
    def __init__(self, starting_battery_charge,
                 solar_generation_forecasts, power_usage_predictions,
                 grid_import_prices, grid_export_prices):
        if not isinstance(starting_battery_charge, Energy):
            raise ValueError()
        self.starting_battery_charge = starting_battery_charge

        self.solar_generation_forecasts = list(solar_generation_forecasts)
        self.power_usage_predictions = list(power_usage_predictions)
        self.grid_import_prices = list(grid_import_prices)
        self.grid_export_prices = list(grid_export_prices)

        # TODO make these configurable
        self.min_battery_charge = Energy(kwh=2.9 * 2) * 0.05
        self.max_battery_charge = Energy(kwh=2.9 * 2) * 0.95
        self.max_battery_charge_power = Power(watts=3000)
        self.max_battery_discharge_power = Power(watts=3000)
        self.battery_discharge_cost = EnergyPrice(p_kwh=7)

        assert(self.min_battery_charge <= self.starting_battery_charge <= self.max_battery_charge)

        self.__complete_states = []


    def get_solar_generation(self, start_date, duration):
        forecast = next(f for f in self.solar_generation_forecasts if f.start_date == start_date)
        if forecast.duration != duration:
            raise NotImplementedError("Non-uniform durations not implemented yet")
        return Energy(wm=forecast.power * duration)


    def get_power_usage(self, start_date, duration):
        usage = next(f for f in self.power_usage_predictions if f.start_date == start_date)
        if usage.duration != duration:
            raise NotImplementedError("Non-uniform durations not implemented yet")
        return Energy(wm=usage.power * duration)


    def get_grid_import_price(self, start_date, duration):
        price = next(f for f in self.grid_import_prices if f.start_date == start_date)
        if price.duration != duration:
            raise NotImplementedError("Non-uniform durations not implemented yet")
        return EnergyPrice(p_kwh=price.price)


    def get_grid_export_price(self, start_date, duration):
        price = next(f for f in self.grid_export_prices if f.start_date == start_date)
        if price.duration != duration:
            raise NotImplementedError("Non-uniform durations not implemented yet")
        return EnergyPrice(p_kwh=price.price)


    # TODO Docs everywhere
    # iterate through the windows in time order
    #     for each window with a positive residual (supply exceeds demand), add it to the modelled battery SOC (until the SOC reaches fully charged state)
    #     for each window with a negative residual (demand exceeds supply), add two partially satisfied models to explore from this window onwards:
    #         one where the the residual demand is met by the battery (battery discharge)
    #             if the residual demand exceeds the modelled battery SOC, find the window after the most recent one where the battery SOC exceeds 100% minus the additional battery SOC needed to meet the residual demand (and the window isn’t already marked as charging), add a partially satisfied model to explore from there where the battery charges from the mains during that window
    #         one where the residual demand is met by importing from the grid (no discharge)
    # iterate through the set of models until all of them have iterated through all windows (each of them should always make forward progress)
    # cost each of the models and choose a winner
    def __step_state(self, state):
        solar_generation_forecast = self.solar_generation_forecasts[state.generation]
        power_usage_prediction = self.power_usage_predictions[state.generation]

        assert(solar_generation_forecast.start_date == power_usage_prediction.start_date)
        assert(solar_generation_forecast.duration == power_usage_prediction.duration)

        assert(state.start_date == solar_generation_forecast.start_date)
        assert(state.duration == solar_generation_forecast.duration)

        # Measured in watts
        residual = abs(solar_generation_forecast.power - power_usage_prediction.power)

        residual_battery = min(residual, self.max_battery_discharge_power)
        residual_grid = residual - residual_battery

        if solar_generation_forecast.power >= power_usage_prediction.power:
            # Solar supply exceeds demand. The residual will charge the battery, up
            # to its charge current limit. The rest will be exported to the grid.
            return [ModelState(
                model=self,
                generation = state.generation + 1,
                start_date=state.start_date + datetime.timedelta(minutes=state.duration.minutes()),
                duration=state.duration,
                previous_state=state,
                battery_charge=min(state.battery_charge + residual_battery * state.duration, self.max_battery_charge),
                grid_charging_enabled=False,  # TODO: good idea?
                discharging_enabled=state.discharging_enabled,
            )]

        else:
            # Demand exceeds supply
            states = []

            # A new model state where the residual demand is met by the battery
            # (discharge).
            new_battery_charge_unclamped = state.battery_charge - residual_battery * state.duration
            new_battery_charge = max(new_battery_charge_unclamped, self.min_battery_charge)

            states.append(ModelState(
                model=self,
                generation = state.generation + 1,
                start_date=state.start_date + datetime.timedelta(minutes=state.duration.minutes()),
                duration=state.duration,
                previous_state=state,
                battery_charge=new_battery_charge,
                grid_charging_enabled=False,  # TODO: good idea?
                discharging_enabled=True,
            ))

            # If the residual demand exceeds the modelled battery charge level,
            # find the window after the most recent one where the battery charge
            # exceeds (max_battery_charge minus the additional battery charge
            # needed to meet the residual demand), (and the window isn’t already
            # marked as charging), add a model to explore from there where the
            # battery charges from the grid during that window.
            # TODO: This might be over-restricting the search space.
            if new_battery_charge_unclamped < self.min_battery_charge:
                battery_residual = self.min_battery_charge - new_battery_charge_unclamped

                min_desired_battery_charge = self.max_battery_charge - battery_residual
                state_to_charge = state.find_oldest_matching_state(lambda m: m.battery_charge <= min_desired_battery_charge and not m.grid_charging_enabled)

                if state_to_charge and not state_to_charge.grid_charging_enabled:
                    states.append(ModelState(
                        model=self,
                        generation = state_to_charge.generation + 1,
                        start_date=state_to_charge.start_date,
                        duration=state_to_charge.duration,
                        previous_state=state_to_charge.previous_state,
                        battery_charge=state_to_charge.battery_charge,
                        grid_charging_enabled=True,
                        discharging_enabled=False,  # can’t be enabled at the same time as grid charging
                    ))

            # A new model state where the residual demand is met by importing
            # from the grid (no discharge):
            states.append(ModelState(
                model=self,
                generation = state.generation + 1,
                start_date=state.start_date + datetime.timedelta(minutes=state.duration.minutes()),
                duration=state.duration,
                previous_state=state,
                battery_charge=state.battery_charge,
                grid_charging_enabled=False,  # TODO: good idea?
                discharging_enabled=False,
            ))

            return states


    def generate(self):
        if self.__complete_states:
            return self.__complete_states

        frontier_states = []

        # Add initial frontier
        # TODO: should I be splitting states and transitions?
        solar_generation_forecast = list(self.solar_generation_forecasts)[0]
        power_usage_forecast = list(self.power_usage_predictions)[0]

        frontier_states = [ModelState(
            model=self,
            generation=0,
            start_date=solar_generation_forecast.start_date,
            duration=solar_generation_forecast.duration,
            previous_state=None,
            battery_charge=self.starting_battery_charge,
            grid_charging_enabled=grid_charging_enabled,
            discharging_enabled=discharging_enabled,
        ) for (grid_charging_enabled, discharging_enabled) in product([True, False], repeat=2) if not grid_charging_enabled or not discharging_enabled]

        # Push the frontier forward until there’s nothing left to do
        while frontier_states:
            new_states = []
            for m in frontier_states:
                new_states.extend(self.__step_state(m))

            frontier_states = []
            for s in new_states:
                if s.start_date >= self.solar_generation_forecasts[-1].start_date:
                    self.__complete_states.append(s)
                else:
                    frontier_states.append(s)

        return self.__complete_states


    def __cost_state(self, state):
        cost = 0
        while state:
            cost += state.grid_import * self.get_grid_import_price(state.start_date, state.duration) - \
                    state.grid_export * self.get_grid_export_price(state.start_date, state.duration) + \
                    state.battery_discharge * self.battery_discharge_cost
            state = state.previous_state

        return cost


    def lowest_cost(self):
        complete_states = self.generate()
        costs = [(self.__cost_state(s), s) for s in complete_states]
        return sorted(costs, key=itemgetter(0))[0]
