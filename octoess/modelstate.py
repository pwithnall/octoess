#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2020 Philip Withnall
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA


import datetime
from .units import *


class ModelState:
    def __init__(self, model, generation, start_date, duration, previous_state,
                 battery_charge, grid_charging_enabled, discharging_enabled):
        if not (isinstance(start_date, datetime.datetime) and
                isinstance(duration, Duration) and
                (previous_state is None or isinstance(previous_state, ModelState)) and
                isinstance(battery_charge, Energy)):
            raise ValueError()

        # Model properties
        self.model = model
        self.generation = generation
        self.start_date = start_date  # datetime
        self.duration = duration  # Duration
        self.previous_state = previous_state  # ModelState

        assert(self.generation == (self.previous_state.generation + 1 if self.previous_state else 0))

        # Battery state and configuration
        # TODO: figure out a better solution for type safety
        self.battery_charge = battery_charge
        self.grid_charging_enabled = grid_charging_enabled  # boolean
        self.discharging_enabled = discharging_enabled  # boolean

        assert(not self.discharging_enabled or not self.grid_charging_enabled)
        assert(self.model.min_battery_charge <= self.battery_charge <= self.model.max_battery_charge)

        # Cached cost function variables
        self.__costs_calculated = False
        self.__grid_import = Energy(wm=0)
        self.__grid_export = Energy(wm=0)
        self.__battery_discharge = Energy(wm=0)


    def find_oldest_matching_state(self, condition):
        if not condition(self):
            return None

        matching_state = self
        while matching_state.previous_state and condition(matching_state.previous_state):
            matching_state = matching_state.previous_state

        return matching_state


    def __ensure_calculate_costs(self):
        if self.__costs_calculated:
            return

        solar_generation = \
            Energy(wm=self.model.get_solar_generation(self.start_date, self.duration))
        power_usage = \
            Energy(wm=self.model.get_power_usage(self.start_date, self.duration))

        residual = abs(solar_generation - power_usage)

        if solar_generation >= power_usage:
            # watt-minutes
            residual_battery = min(residual, self.model.max_battery_charge_power * self.duration)
            residual_grid = residual - residual_battery

            if not self.grid_charging_enabled:
                grid_import = Energy(wm=0)
            else:
                raise NotImplementedError("Grid charging when solar is high is not yet implemented")

            grid_export = residual_grid
            battery_discharge = Energy(wm=0)
        else:
            # watt-minutes
            if self.discharging_enabled:
                residual_battery = min(residual, self.model.max_battery_discharge_power * self.duration)
            else:
                residual_battery = Energy(wm=0)
            residual_grid = residual - residual_battery
            new_battery_charge = max(self.battery_charge - residual_battery, self.model.min_battery_charge)

            if not self.grid_charging_enabled:
                grid_import = residual_grid
            else:
                # TODO this should be subject to the charge current limit
                grid_import = residual_grid + self.model.max_battery_charge - self.battery_charge

            grid_export = Energy(wm=0)

            if self.discharging_enabled:
                # TODO this should be subject to the discharge current limit
                battery_discharge = self.battery_charge - new_battery_charge
            else:
                battery_discharge = Energy(wm=0)

        self.__grid_import = grid_import
        self.__grid_export = grid_export
        self.__battery_discharge = battery_discharge

        self.__costs_calculated = True


    # watt-minutes, integer
    @property
    def grid_import(self):
        self.__ensure_calculate_costs()
        return self.__grid_import


    # watt-minutes, integer
    @property
    def grid_export(self):
        self.__ensure_calculate_costs()
        return self.__grid_export


    # watt-minutes, integer
    @property
    def battery_discharge(self):
        self.__ensure_calculate_costs()
        return self.__battery_discharge

