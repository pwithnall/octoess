#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2020 Philip Withnall
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA


import alphaess
import argparse
from collections import namedtuple
import configparser
import datetime
import dateutil.parser
import json
import pickle
import requests

from octoess import Duration, Energy, EnergyPrice, Model, ModelState, Power


UtilityRate = namedtuple(
    'UtilityRate',
    ['start_date', 'duration', 'price']
)

def get_utility_rates(import_tariff, tariff_region):
    # TODO https://api.octopus.energy/v1/products/AGILE-18-02-21/electricity-tariffs/E-1R-AGILE-18-02-21-A/standard-unit-rates/
    url = f'https://api.octopus.energy/v1/products/{import_tariff}/electricity-tariffs/E-1R-{import_tariff}-{tariff_region}/standard-unit-rates'
    response = requests.get(url)
    response.raise_for_status()
    response_json = response.json()
    data = response_json['results']

    # Parse it all
    def __parse_utility_rate(entry):
        valid_from = dateutil.parser.isoparse(entry['valid_from'])
        valid_to = dateutil.parser.isoparse(entry['valid_to'])
        # TODO hack because the rest of the data is old
        diff = datetime.timedelta(days=6, hours=-2)
        return UtilityRate(
            start_date=valid_from - diff,
            duration=Duration(seconds=(valid_to - valid_from).total_seconds()),
            price=EnergyPrice(p_kwh=entry['value_inc_vat']))

    data = map(__parse_utility_rate, data)

    return data


SolarGenerationForecast = namedtuple(
    'SolarGenerationForecast',
    ['start_date', 'duration', 'power']
)

def get_solar_forecasts(solcast_site_id, solcast_api_key):
    # TODO temporary cache
    try:
        with open('solar-cache.json', 'r') as fd:
            response_json = json.load(fd)
    except FileNotFoundError:
        url = f'https://api.solcast.com.au/rooftop_sites/{solcast_site_id}/forecasts?format=json'
        headers = { 'Authorization': f'Bearer {solcast_api_key}' }
        response = requests.get(url, headers=headers)
        response.raise_for_status()
        response_json = response.json()

    # TODO limit to the right date range and frequency
    forecasts = response_json['forecasts']
    forecasts = forecasts[:48]

    # Parse it all
    # TODO currently don’t use pv_estimate10 or pv_estimate90 confidence intervals
    def __parse_solar_generation_forecast(entry):
        duration = 30 if entry['period'] == 'PT30M' else -1
        date_str = entry['period_end'].replace('.0000000Z', 'Z')
        return SolarGenerationForecast(
            start_date=dateutil.parser.isoparse(date_str) - datetime.timedelta(minutes=duration),
            duration=Duration(minutes=duration),
            power=Power(watts=entry['pv_estimate'] * 1000))

    forecasts = map(__parse_solar_generation_forecast, forecasts)

    return forecasts


PowerUsagePrediction = namedtuple(
    'PowerUsagePrediction',
    ['start_date', 'duration', 'power']
)

def predict_power_usage(session):
    # TODO: horrible caching, and this is actually just historic data, not a prediction
    try:
        with open('power-usage-cache.pkl', 'rb') as fd:
            data = pickle.load(fd)
    except FileNotFoundError:
        system_list = session.get_system_list()

        for system in system_list:
            data = system.get_power_data(datetime.date(2020, 5, 13))
        with open('power-usage-cache.pkl', 'wb') as fd:
            pickle.dump(data, fd)

    # TODO: filter data to the right date range and frequency
    data = data[::6]

    # Parse it all
    def __parse_power_usage_prediction(entry):
        # TODO hack because the data is old
        diff = datetime.timedelta(days=15, hours=-2)
        return PowerUsagePrediction(
            start_date=dateutil.parser.isoparse(entry[0] + 'Z') + diff,
            duration=Duration(minutes=30),
            power=Power(watts=entry[4]))

    data = map(__parse_power_usage_prediction, data)

    return data


def wm_to_kwh(wm):
    return wm / 60.0 / 1000.0


def format_state_as_dot(state, i):
    charge_percent = state.battery_charge / state.model.max_battery_charge * 100.0
    solar_generation = \
        state.model.get_solar_generation(state.start_date, state.duration)
    power_usage = \
        state.model.get_power_usage(state.start_date, state.duration)

    label = state.start_date.isoformat() + '\\n'
    label += f'Battery charge: {charge_percent}%\\n'
    if not state.previous_state or state.grid_charging_enabled != state.previous_state.grid_charging_enabled:
        label += 'Grid charging ' + ('enabled' if state.grid_charging_enabled else 'disabled') + '\\n'
    if not state.previous_state or state.discharging_enabled != state.previous_state.discharging_enabled:
        label += 'Discharging ' + ('enabled' if state.discharging_enabled else 'disabled') + '\\n'
    label += f'Solar generation: {wm_to_kwh(solar_generation)}kWh\\n'
    label += f'Power usage: {wm_to_kwh(power_usage)}kWh\\n'
    label += f'Grid import: {wm_to_kwh(state.grid_import)}kWh\\n'
    label += f'Grid export: {wm_to_kwh(state.grid_export)}kWh\\n'
    label += f'Battery discharge: {wm_to_kwh(state.battery_discharge)}kWh\\n'

    return f'  state{i} [shape=box, label="{label}"];\n'


def print_dot(final_state):
    with open('out.dot', 'w') as fd:
        fd.write('digraph out {\n')

        # Output each state leading back to the starting state
        state = final_state
        i = 0
        while state.previous_state:
            fd.write(format_state_as_dot(state, i))
            # TODO there's probably a more pythonic way to iterate back through the graph
            state = state.previous_state
            i = i + 1

        # Output the starting state
        fd.write(format_state_as_dot(state, i))

        # Link the nodes
        fd.write('  ' + ' -> '.join(f'state{x}' for x in reversed(range(i + 1))) + ';\n')

        fd.write('}')


def main():
    username = None
    password = None

    try:
        config = configparser.ConfigParser()
        config.read('credentials.ini')
        username = config['alphaess.com']['username']
        password = config['alphaess.com']['password']
        import_tariff = config['octopus.energy']['import-tariff']
        export_tariff = config['octopus.energy']['export-tariff']
        tariff_region = config['octopus.energy']['tariff-region']
        grid_charge_high_cap = float(config['policy']['grid-charge-high-cap'])
        solcast_site_id = config['solcast.com.au']['site-id']
        solcast_api_key = config['solcast.com.au']['api-key']
    except:
        pass

    parser = argparse.ArgumentParser(description='Dump alphaess battery data')
    parser.add_argument('--username', '-u', type=str, default=username,
                        required=(username is None),
                        help='account username (default: loaded from credentials.ini)')
    parser.add_argument('--password', '-p', type=str, default=password,
                        required=(password is None),
                        help='account password (default: loaded from credentials.ini)')
    parser.add_argument('--import-tariff', '-i', type=str, default=import_tariff,
                        required=(import_tariff is None),
                        help='Octopus import tariff product code (default: loaded from credentials.ini)')
    parser.add_argument('--export-tariff', '-e', type=str, default=export_tariff,
                        required=(export_tariff is None),
                        help='Octopus export tariff product code (default: loaded from credentials.ini)')
    parser.add_argument('--grid-charge-high-cap', type=float, default=grid_charge_high_cap,
                        required=(grid_charge_high_cap is None),
                        help='maximum battery charge level (0.0–1.0) when charging from grid (default: loaded from credentials.ini)')
    parser.add_argument('--solcast-site-id', type=str, default=solcast_site_id,
                        required=(solcast_site_id is None),
                        help='site ID for Solcast rooftop solar predictions (default: loaded from credentials.ini)')
    # TODO solcast_api_key

    args = parser.parse_args()

    # Grab the Octopus tariff data
    grid_import_prices = get_utility_rates(import_tariff, tariff_region)
    grid_export_prices = get_utility_rates(export_tariff, tariff_region)

    # Grab the solar prediction data
    solar_generation_forecasts = get_solar_forecasts(solcast_site_id, solcast_api_key)

    # Create an alphaess session and log in
    # TODO
    session = None
    #session = alphaess.Session()
    #session.login(args.username, args.password)

    # Grab the power usage predictions
    power_usage_predictions = predict_power_usage(session)

    # Generate model
    starting_battery_charge = Energy(kwh=2.9 * 2) * 0.2
    model = Model(starting_battery_charge,
                  solar_generation_forecasts, power_usage_predictions,
                  grid_import_prices, grid_export_prices)
    (final_cost, final_state) = model.lowest_cost()
    print(final_cost, final_state)

    print_dot(final_state)

    return  # TODO

    # Find the two periods (if any) where the price is the most negative
    # TODO: need to coalesce adjacent periods
    negative_periods = sorted([r for r in rates if r['value_inc_vat'] < 0.0],
                              key=lambda r: r['value_inc_vat'])

    def __extract_time(date_str):
        date = dateutil.parser.parse(date_str)
        return date.hour * 3600 + date.minute * 60 + date.second

    grid_charge_periods = [(__extract_time(r['valid_from']),
                            __extract_time(r['valid_to'])) for r in negative_periods]

    # TODO after here
    return

    # Set each battery to charge from the grid whenever the price is negative
    system_list = session.get_system_list()

    for system in system_list:
        system.set_charge_periods(grid_charge_periods[0:2],
                                  grid_charge_high_cap)


if __name__ == '__main__':
    main()
